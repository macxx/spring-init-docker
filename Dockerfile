FROM openjdk:9.0.1-jre

ADD build/libs/project-0.0.1-SNAPSHOT.jar /var/local/share/demo/

ENTRYPOINT java -jar /var/local/share/demo/project-0.0.1-SNAPSHOT.jar --server.port=8088

EXPOSE 8088
